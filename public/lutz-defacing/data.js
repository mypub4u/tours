var APP_DATA = {
  "scenes": [
    {
      "id": "0-lutz-unmasked",
      "name": "Lutz unmasked",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "yaw": -1.2833844647970896,
        "pitch": -0.16372776002214806,
        "fov": 1.4825054537253635
      },
      "linkHotspots": [
        {
          "yaw": -1.0792492846992943,
          "pitch": -0.6737629645413072,
          "rotation": 1.5707963267948966,
          "target": "1-lutz-deface-default"
        },
        {
          "yaw": -1.4948170346091132,
          "pitch": -0.5876657028488452,
          "rotation": 4.71238898038469,
          "target": "2-lutz-deface-blackbox"
        },
        {
          "yaw": -1.5447485222213189,
          "pitch": -0.31312106546661056,
          "rotation": 4.71238898038469,
          "target": "3-lutz-deface-groovy"
        },
        {
          "yaw": -1.0333720675807925,
          "pitch": -0.17105747411568473,
          "rotation": 1.5707963267948966,
          "target": "4-lutz-deface-serious-smiley"
        },
        {
          "yaw": -1.6387612009488794,
          "pitch": -0.035507123125064766,
          "rotation": 4.71238898038469,
          "target": "5-lutz-deface-ano"
        }
      ],
      "infoHotspots": [
        {
          "yaw": -1.3599482670023207,
          "pitch": -0.65995096477066355,
          "title": "<a href='https://codefreezr.gitlab.io/chaoslab/WIRtuell/' target='new'>WIRtuell - Rundgang</a>",
          "text": "Click to continue"
        }
      ]
    },
    {
      "id": "1-lutz-deface-default",
      "name": "Lutz deface default",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "yaw": -1.0987750730872996,
        "pitch": -0.33686249576797067,
        "fov": 1.4825054537253635
      },
      "linkHotspots": [
        {
          "yaw": -1.0568281788503242,
          "pitch": -0.6891151210839137,
          "rotation": 6.283185307179586,
          "target": "0-lutz-unmasked"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-lutz-deface-blackbox",
      "name": "Lutz deface blackbox",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "yaw": -1.2714728430134734,
        "pitch": -0.20001949126667462,
        "fov": 1.4825054537253635
      },
      "linkHotspots": [
        {
          "yaw": -1.4923407127405781,
          "pitch": -0.5500994098510859,
          "rotation": 0,
          "target": "0-lutz-unmasked"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-lutz-deface-groovy",
      "name": "Lutz deface groovy",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "yaw": -1.046618028478715,
        "pitch": -0.2406160684056715,
        "fov": 1.4825054537253635
      },
      "linkHotspots": [
        {
          "yaw": -1.5388057634353505,
          "pitch": -0.3021768106452001,
          "rotation": 0,
          "target": "0-lutz-unmasked"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-lutz-deface-serious-smiley",
      "name": "Lutz deface serious smiley",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "yaw": -1.3155165695718196,
        "pitch": -0.15057908796999797,
        "fov": 1.4825054537253635
      },
      "linkHotspots": [
        {
          "yaw": -0.9948278222437494,
          "pitch": -0.1174978861873548,
          "rotation": 0,
          "target": "0-lutz-unmasked"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-lutz-deface-ano",
      "name": "Lutz deface ano",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        }
      ],
      "faceSize": 320,
      "initialViewParameters": {
        "yaw": -1.2401786162483681,
        "pitch": -0.19404521645620498,
        "fov": 1.4825054537253635
      },
      "linkHotspots": [
        {
          "yaw": -1.6232318391842533,
          "pitch": -0.049280124347767185,
          "rotation": 0,
          "target": "0-lutz-unmasked"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "Project Title",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
