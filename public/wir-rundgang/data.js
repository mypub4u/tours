var APP_DATA = {
  "scenes": [
    {
      "id": "0-wir-haus-seite",
      "name": "WIR-Haus-Seite",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.2972321274214256,
        "pitch": -0.43083760586254627,
        "fov": 1.5877756795056996
      },
      "linkHotspots": [
        {
          "yaw": 1.6854861808123225,
          "pitch": 0.1461742351880364,
          "rotation": 0,
          "target": "1-eingang-aussen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-eingang-aussen",
      "name": "Eingang-aussen",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.8805821418234885,
        "pitch": 0.16349480951983253,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.401188322071822,
          "pitch": 0.22318165188042727,
          "rotation": 5.497787143782138,
          "target": "0-wir-haus-seite"
        },
        {
          "yaw": -1.9731522167916413,
          "pitch": 0.003244116322363766,
          "rotation": 5.497787143782138,
          "target": "2-brunnen-hinterhof"
        },
        {
          "yaw": -2.983361929018333,
          "pitch": -0.00009239983268116703,
          "rotation": 6.283185307179586,
          "target": "4-foyer"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-brunnen-hinterhof",
      "name": "Brunnen-Hinterhof",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.2494831868650067,
        "pitch": 0.1316030209628387,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.7863361192695759,
          "pitch": -0.022879886286634843,
          "rotation": 0.7853981633974483,
          "target": "1-eingang-aussen"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-keller-flur",
      "name": "Keller-Flur",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.083722810779463,
        "pitch": 0.33926952128744503,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.15779946044306747,
          "pitch": 0.0565886137412388,
          "rotation": 5.497787143782138,
          "target": "8-eg-flur-hinten"
        }
      ],
      "infoHotspots": [
        {
          "yaw": 0.9487348801015347,
          "pitch": 0.24926781843257828,
          "title": "Hier geht's zum Freifunk<br>",
          "text": "Text"
        }
      ]
    },
    {
      "id": "4-foyer",
      "name": "Foyer",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.0066521984861652,
        "pitch": 0.28768723021624965,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.8896674350496996,
          "pitch": 0.41789380647458074,
          "rotation": 0,
          "target": "1-eingang-aussen"
        },
        {
          "yaw": -1.006575848611814,
          "pitch": 0.41789380647458074,
          "rotation": 0,
          "target": "5-eg-eingang"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-eg-eingang",
      "name": "EG-Eingang",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.162613638257758,
        "pitch": 0.27742481919979767,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.1386582598913506,
          "pitch": 0.08068160614582709,
          "rotation": 7.0685834705770345,
          "target": "1-eingang-aussen"
        },
        {
          "yaw": -2.0733153587497775,
          "pitch": 0.5896340516548157,
          "rotation": 11.780972450961727,
          "target": "4-foyer"
        },
        {
          "yaw": -0.9847312322849753,
          "pitch": 0.2651223917039456,
          "rotation": 0.7853981633974483,
          "target": "6-saal"
        },
        {
          "yaw": -0.3240830339187717,
          "pitch": 0.5198952487957911,
          "rotation": 14.137166941154074,
          "target": "7-eg-flur"
        },
        {
          "yaw": 1.0697243389197197,
          "pitch": -0.3225518965673224,
          "rotation": 5.497787143782138,
          "target": "9-1og-flur"
        },
        {
          "yaw": 0.4244656790286534,
          "pitch": 0.11265469862718014,
          "rotation": 4.71238898038469,
          "target": "6-saal"
        },

        {
          "yaw": 0.6196943640906145,
          "pitch": 0.012155102373746729,
          "rotation": 0,
          "target": "21-multifunktionsraum"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-saal",
      "name": "Saal",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.2443889572119673,
        "pitch": 0.19755077586047776,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.578786715758513,
          "pitch": 0.0597885635653288,
          "rotation": 4.71238898038469,
          "target": "5-eg-eingang"
        },
        {
          "yaw": 1.4131111150302313,
          "pitch": 0.17599795231837945,
          "rotation": 5.497787143782138,
          "target": "7-eg-flur"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-eg-flur",
      "name": "EG-Flur",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.5575365047946752,
        "pitch": 0.2052904746643911,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.9838165858559496,
          "pitch": 0.10415454650474132,
          "rotation": 1.5707963267948966,
          "target": "6-saal"
        },
        {
          "yaw": 1.7252021771917558,
          "pitch": 0.43879810910828354,
          "rotation": 0,
          "target": "5-eg-eingang"
        },
        {
          "yaw": 1.7186276156559925,
          "pitch": 0.27058253254592124,
          "rotation": 0,
          "target": "4-foyer"
        },
        {
          "yaw": 1.7053766994818274,
          "pitch": 0.11351319386662873,
          "rotation": 0,
          "target": "1-eingang-aussen"
        },
        {
          "yaw": 0.12399617022879437,
          "pitch": 0.4918853511837753,
          "rotation": 0,
          "target": "8-eg-flur-hinten"
        },
        {
          "yaw": 0.6482262095069373,
          "pitch": -0.09577769340115339,
          "rotation": 5.497787143782138,
          "target": "9-1og-flur"
        },
        {
          "yaw": -1.379729082566218,
          "pitch": 0.2348035113938849,
          "rotation": 0,
          "target": "21-multifunktionsraum"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-eg-flur-hinten",
      "name": "EG-Flur-hinten",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 2.4771438613263133,
        "pitch": 0.2792687361630968,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.5720958822314852,
          "pitch": 0.2893108191102982,
          "rotation": 5.497787143782138,
          "target": "7-eg-flur"
        },
 
        {
          "yaw": 2.905740822615682,
          "pitch": 0.6567623247281649,
          "rotation": 2.356194490192345,
          "target": "3-keller-flur"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "9-1og-flur",
      "name": "1OG-Flur",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.9224171151715552,
        "pitch": 0.4970190439242934,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.8968008061938555,
          "pitch": 0.5340718794143218,
          "rotation": 2.356194490192345,
          "target": "7-eg-flur"
        },
        {
          "yaw": -1.635337451481453,
          "pitch": 0.5169529367366152,
          "rotation": 2.356194490192345,
          "target": "5-eg-eingang"
        },
        {
          "yaw": -1.3845876463060165,
          "pitch": -0.031388994992145314,
          "rotation": 5.497787143782138,
          "target": "10-2og-flur2"
        },
        {
          "yaw": 0.25367968358484916,
          "pitch": 0.51306620527631,
          "rotation": 1.5707963267948966,
          "target": "15-kueche"
        },
        {
          "yaw": 1.6492898663100686,
          "pitch": 0.33400085064652885,
          "rotation": 0,
          "target": "20-ideenwerk-lab"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "10-2og-flur2",
      "name": "2OG-Flur2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.1298597459330537,
        "pitch": 0.6473602393061597,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.283373515110501,
          "pitch": 0.6234687960990382,
          "rotation": 2.356194490192345,
          "target": "9-1og-flur"
        },
        {
          "yaw": 0.0521440896761014,
          "pitch": 0.41627426808040013,
          "rotation": 4.71238898038469,
          "target": "11-2og-flur"
        },
        {
          "yaw": 2.512796878286724,
          "pitch": 0.698350297445721,
          "rotation": 1.5707963267948966,
          "target": "13-2og-wickelraum"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "11-2og-flur",
      "name": "2OG-Flur",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.6115893071685505,
        "pitch": 0.3868217833990517,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.02671071440552275,
          "pitch": 0.4353148164659899,
          "rotation": 0,
          "target": "10-2og-flur2"
        },
        {
          "yaw": 1.98968491544609094,
          "pitch": 0.4164517580177894,
          "rotation": 0,
          "target": "12-aufnahmestudio"
        },
        {
          "yaw": -1.5334532569575853,
          "pitch": 0.4839229622527377,
          "rotation": 0,
          "target": "14-konferenzraum-2og"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "12-aufnahmestudio",
      "name": "Aufnahmestudio",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.125727570887186,
        "pitch": 0.3543961878416688,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.4409346617781829,
          "pitch": 0.5808432223413966,
          "rotation": 0,
          "target": "11-2og-flur"
        },
        {
          "yaw": -1.4933931142382484,
          "pitch": 0.16581439574957635,
          "rotation": 10.995574287564278,
          "target": "14-konferenzraum-2og"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "13-2og-wickelraum",
      "name": "2OG-Wickelraum",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.286449226145173,
          "pitch": 0.18827028209725327,
          "rotation": 1.5707963267948966,
          "target": "10-2og-flur2"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "14-konferenzraum-2og",
      "name": "Konferenzraum-2OG",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.621137823603398,
        "pitch": 0.052342691846991585,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.9628393256653913,
          "pitch": 0.4837565544384752,
          "rotation": 0,
          "target": "11-2og-flur"
        },
        {
          "yaw": 3.0106563263465613,
          "pitch": 0.19215099324170737,
          "rotation": 0.7853981633974483,
          "target": "12-aufnahmestudio"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "15-kueche",
      "name": "Kueche",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.4248018844445554,
        "pitch": 0.16207142736657687,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 1.0491861378792642,
          "pitch": 0.21003035438981144,
          "rotation": 4.71238898038469,
          "target": "9-1og-flur"
        },
        {
          "yaw": 2.2668771475823455,
          "pitch": 0.13830556868684418,
          "rotation": 1.5707963267948966,
          "target": "17-garderobe"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "16-zwischenraum",
      "name": "Zwischenraum",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 1.6223049285597435,
        "pitch": -1.1922265274045323,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 0.05,
          "pitch": 0.2369878907645954,
          "rotation": 0,
          "target": "17-garderobe"
        },
        {
          "yaw": 2.8590938584812088,
          "pitch": 0.2344736428448165,
          "rotation": 0,
          "target": "18-ideenwerk-makerspace2"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "17-garderobe",
      "name": "Garderobe",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.8188597876957999,
          "pitch": 0.19132830245981225,
          "rotation": 4.71238898038469,
          "target": "15-kueche"
        },
        {
          "yaw": 0.163587014033574,
          "pitch": 0.44123400398254375,
          "rotation": 0,
          "target": "16-zwischenraum"
        },
        {
          "yaw": 0.027503387540289026,
          "pitch": 0.11499010779408891,
          "rotation": 0,
          "target": "18-ideenwerk-makerspace2"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "18-ideenwerk-makerspace2",
      "name": "Ideenwerk-Makerspace2",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.257998816769325,
        "pitch": 0.16210534867773418,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 2.410157515592004,
          "pitch": 0.2826347916176495,
          "rotation": -5.497787143782138,
          "target": "16-zwischenraum"
        },
        {
          "yaw": -1.9592335869949569,
          "pitch": 0.2862873167651152,
          "rotation": 13.351768777756625,
          "target": "19-ideenwerk-malerspace"
        },
        {
          "yaw": -1.9524548529786845,
          "pitch": 0.16260913804201316,
          "rotation": 5.497787143782138,
          "target": "20-ideenwerk-lab"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "19-ideenwerk-malerspace",
      "name": "Ideenwerk-Makerspace",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.8972216759011378,
        "pitch": 0.05618476148710272,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -1.1383837799956371,
          "pitch": 0.10812283087358843,
          "rotation": 5.497787143782138,
          "target": "18-ideenwerk-makerspace2"
        },
        {
          "yaw": -1.0942203770523093,
          "pitch": 0.32463079199684763,
          "rotation": 7.0685834705770345,
          "target": "20-ideenwerk-lab"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "20-ideenwerk-lab",
      "name": "Ideenwerk-Lab",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.7504532714644085,
        "pitch": 0.256367509435659,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.3667213809689738,
          "pitch": 0.1605604141160697,
          "rotation": 4.71238898038469,
          "target": "19-ideenwerk-malerspace"
        },
        {
          "yaw": -2.1145762390041334,
          "pitch": 0.08172300627735041,
          "rotation": 0.7853981633974483,
          "target": "18-ideenwerk-makerspace2"
        },
        {
          "yaw": -1.4302622763171104,
          "pitch": 0.29381361393873107,
          "rotation": 0.7853981633974483,
          "target": "9-1og-flur"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "21-multifunktionsraum",
      "name": "Multifunktionsraum",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -1.3912929787483712,
        "pitch": 0.2110325810464282,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.4679991721120782,
          "pitch": 0.2377046012062376,
          "rotation": 5.497787143782138,
          "target": "7-eg-flur"
        },
        {
          "yaw": -2.2860280619270537,
          "pitch": 0.18862524191417762,
          "rotation": 5.497787143782138,
          "target": "2-brunnen-hinterhof"
        }
      ],
      "infoHotspots": []
    }
  ],
  "name": "WIRundgang",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": false,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
